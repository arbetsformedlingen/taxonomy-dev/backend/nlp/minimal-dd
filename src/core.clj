(ns core
  (:require
   [uncomplicate.diamond.native]
   [uncomplicate.diamond.tensor :refer [tensor]]))

;; uncomment to trigger nullpointer exception

(set! *print-length* 128)


(tensor 3)
